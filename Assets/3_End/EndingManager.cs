﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class EndingManager : MonoBehaviour {
    public RectTransform tr;
    public Image image;
    public Text name;
    public Text comment;
    public Text gameoverText;

    public Image panel;
    public float speed = 1f;

    int index = 0;
    Datas.FiredPeople fp;
    Animator anim;

    bool trigger = false;

    public Sprite[] Characters;

    Vector3 startPosition;
    // Use this for initialization
    void Start () {
        if(Datas.firedList.Count == 0) { 
            fakeData();
        }
        startPosition = tr.position;
        fp = Datas.firedList[index];
        int id = fp.ImageId;
        image.sprite = Characters[id];
        name.text = fp.Name;
        anim = GetComponent<Animator>();
        gameoverText.text = Datas.EndMessage;

    }
	
	// Update is called once per frame
	void Update () {
        //화면 넘어가면
        if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= (index + 1)) { 
            tr.position = startPosition;
            index++;
            fp = Datas.firedList[index% Datas.firedList.Count];
            int id = fp.ImageId;
            image.sprite = Characters[id];
            name.text = fp.Name;
        }

        if (!trigger && Input.GetMouseButton(0))
        {
            trigger = true;
        }
        //한번이라도 클릭하면 화면 전환
        if (trigger)
        {
            panel.color += new Color(0, 0, 0, Time.deltaTime * speed);

            if (panel.color.a >= 0.99)
                SceneManager.LoadScene(0);
        }
    }

    void fakeData()
    {
        Datas.firedList.Add(new Datas.FiredPeople(0, "김대리"));
        Datas.firedList.Add(new Datas.FiredPeople(0, "이사원"));
        Datas.firedList.Add(new Datas.FiredPeople(0, "박사원"));
        Datas.firedList.Add(new Datas.FiredPeople(0, "김사장"));
        Datas.firedList.Add(new Datas.FiredPeople(0, "박사장"));

    }
}
