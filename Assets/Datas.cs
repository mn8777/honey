﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Datas{

    public class FiredPeople
    {
        public int ImageId; //0~99번 이미지중 id
        public string Name; //이름

        public FiredPeople(int Id, string Name)
        {
            this.ImageId = Id;
            this.Name = Name;
        }
    }

    public static List<FiredPeople> firedList = new List<FiredPeople>();

    public static string EndMessage = "파 업";
}
