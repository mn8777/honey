﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Talk : MonoBehaviour 
{
	public TextMesh Text;

	public List<string> TalkIndex;

	float Timer = 1.0f;

	public enum WordTypes
	{
		Advance,
		Fire,
		Strike,
	};
	public WordTypes Type;

	void Start()
	{
		if (Type == WordTypes.Advance) 
		{
			TalkIndex.Add ("아이고 사장님\n감사합니다");
			TalkIndex.Add ("사장님\n사랑합니다아");
			TalkIndex.Add ("사장님을 향한\n나의 사랑은 무조건이야~");
			TalkIndex.Add ("충성을\n다하겠습니다!");
			TalkIndex.Add ("얘들아! 아빠가\n치킨 사왔다!");
		} 
		else if (Type == WordTypes.Fire) 
		{
			TalkIndex.Add ("그렇게 사는 거 아니다!");
			TalkIndex.Add ("우리 둘째가\n이제 돌입니다 ㅠㅠ");
			TalkIndex.Add ("얘들아...아빠가\n치킨 사왔다...흙흙ㅠㅠ");
			TalkIndex.Add ("여보 미안해...");
		} 
		else 
		{
			TalkIndex.Add ("우리는 죄짓기 위해\n사는 것이 아닙니다!");
			TalkIndex.Add ("노조 탄압하는\n사장은 사퇴하라!");
			TalkIndex.Add ("민중투쟁!");
			TalkIndex.Add ("부실경영!\n경영진 물러나라!");
			TalkIndex.Add ("열정 대신\n돈으로 주세요!");
		}
		Text.text = TalkIndex [Random.Range (0, TalkIndex.Count)];
	}

	void Update()
	{
		Timer -= Time.deltaTime;
		if (Timer <= 0) 
		{
			Destroy (gameObject);
		}
	}
}
