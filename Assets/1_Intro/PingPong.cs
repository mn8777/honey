﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PingPong : MonoBehaviour {

    Image image;
    public Color c1 = new Color(255, 255, 255, 255);
    public Color c2 = new Color(255, 255, 255, 0);

    float count;

    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
        count = 0;
        
    }
	

    void Update()
    {
        count += Time.deltaTime;
        if(count>1)
            image.color = Color.Lerp(c1, c2, Mathf.PingPong(Time.time, 1));
    }
}
