﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class IntroControl : MonoBehaviour {

    public Image panel;
    public float speed = 1f;

    bool trigger = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!trigger && Input.GetMouseButtonDown(0)) {
            trigger = true;
        }

        //한번이라도 클릭하면 화면 전환
        if (trigger)
        {
            Debug.Log(Time.deltaTime * speed);
            Debug.Log(panel.color);
            panel.color += new Color(0, 0, 0, Time.deltaTime * speed);

            if(panel.color.a >= 0.99)
                SceneManager.LoadScene("Strike", LoadSceneMode.Single);
        }
    }
}
