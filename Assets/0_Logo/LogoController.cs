﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LogoController : MonoBehaviour {

    public Image image;

    float timer;
	// Use this for initialization
	void Start () {
        Screen.SetResolution(768, 1024, false);
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer < 2)
            image.color = new Color(0, 0, 0, (2 - timer)/2);
        else if (timer < 4)
            image.color = new Color(0, 0, 0, (timer-2)/2);
        else
            SceneManager.LoadScene(1);
	}
}
