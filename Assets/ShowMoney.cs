﻿using UnityEngine;
using System.Collections;

public class ShowMoney : MonoBehaviour 
{
	public GameManager Manager;

	public SpriteRenderer[] Numbers;
	public Sprite[] NumberFonts;

	void Update()
	{
		int Money = Manager.Money;

		for (int i = 0; i < 5; i++) 
		{
			Numbers [i].sprite = NumberFonts [Money % 10];
			Money = Money / 10;
		}
	}
}