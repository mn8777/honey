﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Worker : MonoBehaviour
{
	public GameManager Manager;
	public Slot MySlot;
	public SpriteRenderer[] MyRenderers;
	public const float TurnTime = 5.0f;
	const float ClickTime = 0.2f;
	public float ClickValid;

	public string FullName;
	public List<string> FamilyNames;
	string FamilyName;
	public List<string> GradeNames;
	string GradeName;

    public int ImageId;
    public int Star;
	int EXP;
	public int Product;
	public int Anger;

	bool Disappearing;
	public bool strike;

	Vector2 OriginalPosition;

    //분노 게이지 위한 변수들
    public Transform overlap;
    float height;
    Vector3 overlapPosition;
    Vector2 overlapScale;

    //돈나가는 이펙트와 소리 출력용
    public GameObject moneyAni;

	//카드 들고 내려놓을떄 소리
	bool Clicked;
	bool Grabbed;

    //카드 그리기
    //스타
    public StarController starController;
    //숫자
    public Sprite[] num;
    public SpriteRenderer[] coinNum;   //0-1의 자리, 1-10의 자리, 2-100의 자리
    public SpriteRenderer[] productNum;   //0-1의 자리, 1-10의 자리, 2-100의 자리

	public Sprite[] Characters;

	public ShowContribute ShowConPrefab;
	public ShowContribute show;

    void Start()
	{
		FamilyName = FamilyNames [Random.Range (0, 5)];
		FullName = FamilyName+GradeNames[0];
		Product = Random.Range(40, 51);
        ImageId = Random.Range(0, Characters.Length);
        MyRenderers [0].sprite = Characters [ImageId];
        
		OriginalPosition = new Vector2 (transform.position.x, transform.position.y);

        height = -overlap.localPosition.y;
        overlapPosition = overlap.localPosition;
        overlapScale = overlap.localScale;

        //초기 Star까지 다켜기
        for(int i=1;i <= Star; i++)
		{ 
            starController.TurnOn(i);
        }
        //처음에는 1의 자리 0만 키고, 나머지는 꺼둠
        coinNum[0].sprite = num[0];
        coinNum[0].enabled = true;
        productNum[0].sprite = num[0];
        productNum[0].enabled = true;
        for (int i = 1; i < 3; i++)
        {
            coinNum[i].enabled = false;
            productNum[i].enabled = false;
        }
        SetNumber(Product, productNum);

        SetNumber(MySlot.Wage, coinNum);

		InvokeRepeating ("UpdatePerTurn", TurnTime, TurnTime);
	}
	void Update()
	{
		if (ClickValid > 0) 
		{
			ClickValid -= Time.deltaTime;
		}
		else 
		{
			ClickValid = 0;
		}

		if (strike && !Clicked) 
		{
			transform.position = new Vector3 (OriginalPosition.x + Random.Range (-0.1f, 0.1f), OriginalPosition.y + Random.Range (-0.1f, 0.1f), 0);
		}

		if (Disappearing) 
		{
			transform.Rotate (0.0f, 0.0f, Time.deltaTime * 1000.0f);
			foreach (SpriteRenderer renderer in MyRenderers) 
			{
				Color color = renderer.color;
				color.a -= 2*Time.deltaTime;
				renderer.color = color;
			}
		}
	}

	void UpdatePerTurn()
	{
		if (strike == false) 
		{
			EXP += Random.Range(1,5);

			int AngerBonus = 0;

			foreach (Slot slot in MySlot.AdjoinSlots) 
			{
				if (slot.MyWorker != null && slot.MyWorker.strike == true)
				{
					AngerBonus += 1;
				}
			}
			if (Star > MySlot.Level)
			{
				AngerBonus += 1;
			}

			int AngerChange = Random.Range (1, 21);

			for (int i = 0; i < AngerBonus; i++)
			{
				AngerChange *= 3;
			}
			for (int i = 0; i < AngerBonus; i++)
			{
				AngerChange /= 2;
			}
			Anger += AngerChange;

            UpdateAnger();

            if (Anger >= 100)
			{
				Anger = 100;
				strike = true;
				Talk word = Instantiate (Manager.TalkPrefab, transform.position+new Vector3(1,1,-1), Quaternion.identity) as Talk;
				word.Type = Talk.WordTypes.Strike;
			}
			else if (EXP == 10)
			{
				EXP = 0;
				Star += 1;
                //Star 올라가면 켜기
                starController.TurnOn(Star);
				Instantiate (Manager.StarParticle, transform.position+new Vector3(0,0,-1), Quaternion.identity);
			}

			show = Instantiate (ShowConPrefab, transform.position+new Vector3(0.3f,0.9f, 0), Quaternion.identity) as ShowContribute;
			if (Product - MySlot.Wage >= 0) 
			{
				show.Contribution = Product - MySlot.Wage;
			} 
			else
			{
				show.Contribution = 0;
			}
		}
	}
		
	Vector2 InitialMousePosition;

	void OnMouseDown()
	{
		InitialMousePosition = Camera.main.ScreenToWorldPoint (new Vector2 (Input.mousePosition.x, Input.mousePosition.y));
		ClickValid = ClickTime;

		Clicked = true;
    }

	void OnMouseDrag()
	{
        if (!Grabbed && ClickValid == 0)
        {
            Grabbed = true;
			Manager.Trash.sprite = Manager.OpenedTrash;
            AudioSource.PlayClipAtPoint(Manager.GrabSound, Camera.main.transform.position);
        }
        Vector2 WorldPoint = Camera.main.ScreenToWorldPoint (new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		Vector2 PositionOffset = WorldPoint - InitialMousePosition;
		InitialMousePosition = Camera.main.ScreenToWorldPoint (new Vector2(Input.mousePosition.x, Input.mousePosition.y));

		gameObject.transform.position = new Vector3(gameObject.transform.position.x + PositionOffset.x, gameObject.transform.position.y + PositionOffset.y, 0);
	}

	void OnMouseUp()
	{
		Clicked = false;
		Grabbed = false;
		Manager.Trash.sprite = Manager.ClosedTrash;

		if (ClickValid > 0)
		{
			Manager.Money -= 50;
			Anger -= 10;
			if (Anger < 0) 
			{
				Anger = 0;
			}
            UpdateAnger();
            MoneyOut();
			Instantiate (Manager.CoinParticle, Camera.main.ScreenToWorldPoint(Input.mousePosition)-new Vector3(0,0,-1), Quaternion.identity);
        }

		RaycastHit2D[] HitObjects = Physics2D.RaycastAll (transform.position, transform.forward);

		bool Fire = false;
		foreach (RaycastHit2D HitObject in HitObjects) 
		{
			if (HitObject.collider.gameObject.tag == "Garbage") 
			{
				Fire = true;
			}
			else if (HitObject.collider.gameObject.GetComponent<Slot>() != null)
			{
				Slot slot = HitObject.collider.gameObject.GetComponent<Slot>();
				Debug.Log ("RayCast Got Level" + slot.Level + " Slot!");
                Debug.Log(slot.Available);
				if (slot.Level <= Star && slot.Level == MySlot.Level+1 && slot.Available == true)
				{
					MySlot.MyWorker = null;
					MySlot = slot;
					slot.MyWorker = this;
					transform.position = MySlot.transform.position + new Vector3 (0, 0, -1);
                    OriginalPosition = transform.position;
					Product += Random.Range (10, 51);
                    SetNumber(Product, productNum);
					strike = false;
                    Anger = 0;
					Talk word = Instantiate (Manager.TalkPrefab, transform.position+new Vector3(0.5f,0.5f,-1), Quaternion.identity) as Talk;
					word.Type = Talk.WordTypes.Advance;
				}
			}
		}
		if (Fire) 
		{
			foreach (Slot slot in MySlot.AdjoinSlots) 
			{
				if (slot.MyWorker != null) 
				{
					slot.MyWorker.Anger += 30;
				}
			}

			Talk word = Instantiate (Manager.TalkPrefab, transform.position + new Vector3 (1, 1, -1), Quaternion.identity) as Talk;
			word.Type = Talk.WordTypes.Fire;
			AudioSource.PlayClipAtPoint (Manager.FireSound, Camera.main.transform.position);
			MySlot.MyWorker = null;
			Datas.firedList.Add (new Datas.FiredPeople (ImageId, FullName));
			Disappearing = true;
			StartCoroutine (Fired ());
		}
		else 
		{
			gameObject.transform.position = new Vector3 (OriginalPosition.x, OriginalPosition.y, -1);
			if (Grabbed)
			{
				AudioSource.PlayClipAtPoint(Manager.PutSound, Camera.main.transform.position);
			}
		}
	}

	IEnumerator Fired()
	{
		yield return new WaitForSeconds (0.5f);
		Destroy (gameObject);
	}

	public enum Neighbor
	{
		Prev,
		Next,
	}

    //화 업데이트 해서 차오르는 함수
    void UpdateAnger()
    {
        float temp = (float)(Anger / 100.0);
        overlap.localPosition = overlapPosition + new Vector3(0, temp*height,0);
        overlap.localScale = overlapScale + new Vector2(0, temp*4);
        if (overlap.localScale.y > 3.8)
            overlap.localScale = new Vector2(3.8f, 3.8f);
        if (overlap.localPosition.y > 0)
            overlap.localPosition = new Vector3(0, 0, overlap.localPosition.z);
    }

    //돈나가는 이펙트와 소리 출력해주는 함수
    void MoneyOut()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 position = new Vector3(mousePos.x, mousePos.y, 0);  //마우스위치에 생성(그냥 마우스는 z가 -10이라서 0으로 수정)

        GameObject anim = Instantiate(moneyAni, position, Quaternion.identity) as GameObject;
        anim.transform.SetParent(transform);
        Destroy(anim, anim.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).length);

        //소리 재생
        AudioSource.PlayClipAtPoint(Manager.MoneySound, Camera.main.transform.position);
    }
    
    //정수를 입력받아 그 정수를 3개의 SpriteRenderer에 출력해주는 함수
    public void SetNumber(int value,SpriteRenderer[] rd)
    {
        //일의 자리는 무조건 출력
        int d1 = value % 10;
        rd[0].sprite = num[d1];

        value /= 10;
        int d2 = value % 10;
        value /= 10;
        int d3 = value;

        //2의 자리 출력할지 안할지
        if (d3 == 0 && d2 == 0)
            rd[1].enabled = false;
        else
        {
            rd[1].sprite = num[d2];
            rd[1].enabled = true;
        }

        //3의 자리 출력할지 안할지
        if (d3 == 0)
            rd[2].enabled = false;
        else {
            rd[2].sprite = num[d3];
            rd[2].enabled = true;
        }
    }
}