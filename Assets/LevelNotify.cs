﻿using UnityEngine;
using System.Collections;

public class LevelNotify : MonoBehaviour 
{
	public GameManager Manager;
	public SpriteRenderer Renderer;
	public Sprite[] Images;

	public float Lifespan;

	void Update()
	{
		if (Lifespan > 0) 
		{
			Lifespan -= Time.deltaTime;
			if (Lifespan < 1) 
			{
				Vector3 Position = transform.position;
				Position += new Vector3 (-2.4f * Time.deltaTime, 3.0f * Time.deltaTime, 0);
				transform.position = Position;

				Vector3 Scale = transform.localScale;
				Scale -= new Vector3 (1.1f, 1.1f, 0) * Time.deltaTime;
				transform.localScale = Scale;

				transform.Rotate (0.0f, 0.0f, Time.deltaTime * 1000.0f);
			}
		}
		else 
		{
			Lifespan = 0;
			transform.position = new Vector3 (-2.4f, 3.0f, 0.0f);
			transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
		}
	}
}