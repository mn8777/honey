﻿using UnityEngine;
using System.Collections;

public class ShowContribute : MonoBehaviour 
{
	public GameManager Manager;
	public float Lifespan;
	public int Contribution;

	public SpriteRenderer[] Numbers;
	public Sprite[] NumberFonts;

	void Start()
	{
        Numbers[0].sprite = NumberFonts[Contribution % 10];

        int n10;    //혹시나 100의자리수가 넘으면 그냥 9x라고 침
        if (Contribution / 100 > 0)
            n10 = 0;
        else
            n10 = Contribution / 10;
        Numbers[1].sprite = NumberFonts[n10];
        
	}

	void Update()
	{
		Lifespan -= Time.deltaTime;

        //위로 움직이면서 fadeout
        transform.Translate(new Vector3(0, 0.01f, 0));
        for (int i = 0; i < 3; i ++) {
            SpriteRenderer sr = Numbers[i];
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, Lifespan);
        }
		if (Lifespan < 0) 
		{
			Destroy (gameObject);
		}
	}
}
