﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	const float TurnTime = 5.0f;
	public SpriteRenderer BackGround;
	public SpriteRenderer Trash;
	public Sprite OpenedTrash;
	public Sprite ClosedTrash;

	public Sprite[] Buildings;

	public int CompanyLevel;
	public int Money;
	public List<int> LevelPoint;
	public List<Slot> SlotList;

	public AudioClip GrabSound;
	public AudioClip PutSound;
	public AudioClip FireSound;
	public AudioClip MoneySound;

	public ParticleSystem CoinParticle;
	public ParticleSystem StarParticle;
	public LevelNotify NotifierPrefab;
	LevelNotify Notifier;
	public Talk TalkPrefab;

	void Start()
	{
		Screen.SetResolution (768, 1024, false);
		InvokeRepeating ("UpdatePerTurn", TurnTime, TurnTime);
		LevelNotify (1);
        Datas.firedList.Clear();    //게임매니저 새로생길때마다 리스트 초기화
	}

	void Update()
	{

	}

	void UpdatePerTurn()
	{
		int Income = 0;
		int Expense = 0;

		foreach(Slot slot in SlotList)
		{
			if (slot.MyWorker != null) 
			{
				Income += slot.MyWorker.Product;
				Expense += slot.Wage;
				AudioSource.PlayClipAtPoint(MoneySound, Camera.main.transform.position);
			}
		}

		Money += Income-Expense;

		for (int i = 0; i < 4; i++) 
		{
			if (Money >= LevelPoint [i] && CompanyLevel == i+1) 
			{
				CompanyLevel += 1;
				LevelNotify (i + 2);
				BackGround.sprite = Buildings [i+1];
				foreach(Slot slot in SlotList) 
				{
					if (slot.Level == i+2) 
					{
						slot.Available = true;
					}
				}
			}
		}

		//파업 참여자 수를 체크해서 게임오버
		int Workers = 0;
		int Strikes = 0;
		foreach (Slot slot in SlotList) 
		{
			if (slot.MyWorker != null) 
			{
				Workers += 1;
				if(slot.MyWorker.strike) 
				{
					Strikes += 1;
				}
			}
		}
		Debug.Log ("Workers : " + Workers + ", Strikes : " + Strikes);

        bool gameover = false;
		if (Strikes*3 >= Workers*2 && Workers!=0)   //0명일때 파산하는 버그 해결
		{
			Debug.LogError ("총파업 엔딩");
            Datas.EndMessage = "총 파업";
            gameover = true;

        }
		else if(Money <= 0)
		{
			Debug.LogError("파산 엔딩");
            Datas.EndMessage = "파 산";
            gameover = true;
        }

        if (gameover) { 
            SceneManager.LoadScene("End", LoadSceneMode.Single);
        }
    }

	void LevelNotify(int Level)
	{
		Notifier = Instantiate (NotifierPrefab, transform.position - new Vector3 (0, 0, 1), Quaternion.identity) as LevelNotify;
		Notifier.Renderer.sprite = Notifier.Images [Level-1];
		Notifier.Manager = this;
	}
}