﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardController : MonoBehaviour
{
    /*public GameManager Manager;
    public Slot MySlot;
    public float TurnTime = 1f;
    const float ClickTime = 0.2f;
    public float ClickValid;

    public string Name;
    public List<string> FamilyNames;

    int Star;
    int EXP;
    public int Product;
    public int Wage;
    public int Anger;

    public bool strike;

    Vector2 OriginalPosition;

    //분노 게이지 위한 변수들
    public Transform overlap;
    float height;
    Vector3 overlapPosition;
    Vector2 overlapScale;

    //돈나가는 이펙트와 소리 출력용
    public GameObject moneyAni;
    public AudioClip moneySound;
    public AudioClip cardUpSound;
    public AudioClip cardDropSound;

    bool mousePressed = false;  //마우스 꾹 눌렀는지 확인하는 변수

    void Start()
    {
        Name = FamilyNames[Random.Range(0, 5)] + "사원";
        Star = 0;
        EXP = 0;
        Product = Random.Range(40, 51);
        Wage = 20;
        Anger = 0;

        OriginalPosition = new Vector2(transform.position.x, transform.position.y);

        height = -overlap.localPosition.y;
        overlapPosition = overlap.localPosition;
        overlapScale = overlap.localScale;

        InvokeRepeating("UpdatePerTurn", 0.0f, TurnTime);
    }

    void Update()
    {
        if (ClickValid > 0)
        {
            ClickValid -= Time.deltaTime;
        }
        else
        {
            ClickValid = 0;
        }

    }

    void UpdatePerTurn()
    {
        if (strike == false)
        {
            EXP += Random.Range(0, 2);

            int NeighborStrike = 0;
            if (CheckNeighborExist(MySlot.Number, Neighbor.Prev) && Manager.SlotList[MySlot.Number - 1].MyStaff.strike == true)
            {
                NeighborStrike += 1;
            }
            if (CheckNeighborExist(MySlot.Number, Neighbor.Next) && Manager.SlotList[MySlot.Number + 1].MyStaff.strike == true)
            {
                NeighborStrike += 1;
            }

            Anger += Random.Range(1, 11) * (NeighborStrike + 2) / 2;
            UpdateAnger();

            if (Anger >= 100)
            {
                Anger = 100;
                strike = true;
            }
            else if (EXP == 10)
            {
                EXP = 0;
                Star += 1;
            }
        }
    }

    Vector2 InitialMousePosition;

    void OnMouseDown()
    {
        InitialMousePosition = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        ClickValid = ClickTime;

        ////////////////////////////////////////////////////////
        mousePressed = false;
    }

    void OnMouseDrag()
    {
        
        Vector2 WorldPoint = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        Vector2 PositionOffset = WorldPoint - InitialMousePosition;
        InitialMousePosition = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        gameObject.transform.position = new Vector3(gameObject.transform.position.x + PositionOffset.x, gameObject.transform.position.y + PositionOffset.y, 0);


        ////////////////////////////////////////////////////////
        if (!mousePressed && ClickValid == 0)
        {
            PlaySound(cardUpSound);
            mousePressed = true;
        }
    }

    void OnMouseUp()
    {

        Debug.Log(ClickValid);
        if (ClickValid > 0)
        {
            Manager.Money -= 50;
            Anger -= 10;
            if (Anger < 0)
                Anger = 0;
            UpdateAnger();
            MoneyOut();
        }

        RaycastHit2D[] HitObjects = Physics2D.RaycastAll(transform.position, transform.forward);

        bool Fire = false;
        foreach (RaycastHit2D HitObject in HitObjects)
        {
            if (HitObject.collider.gameObject.tag == "Garbage")
            {
                Fire = true;
            }
        }
        if (Fire == true)
        {
            if (CheckNeighborExist(MySlot.Number, Neighbor.Prev))
            {
                Manager.SlotList[MySlot.Number - 1].MyStaff.Anger += 30;
            }
            else if (CheckNeighborExist(MySlot.Number, Neighbor.Next))
            {
                Manager.SlotList[MySlot.Number + 1].MyStaff.Anger += 30;
            }
            Manager.SlotList[MySlot.Number].MyStaff = null;
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("엘스");
            gameObject.transform.position = new Vector3(OriginalPosition.x, OriginalPosition.y, -1);
            ////////////////////////////////////////////////////////
            if (mousePressed) { 
                PlaySound(cardDropSound);
            }
        }
    }

    public enum Neighbor
    {
        Prev,
        Next,
    }
    bool CheckNeighborExist(int Number, Neighbor Direction)
    {
        if (Direction == Neighbor.Prev)
        {
            if (Number != 0 && Manager.SlotList[Number - 1].MyStaff != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (Number != Manager.SlotList.Count - 1 && Manager.SlotList[Number + 1].MyStaff != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //화 업데이트 해서 차오르는 함수
    void UpdateAnger()
    {
        float temp = (float)(Anger / 100.0);
        overlap.localPosition = overlapPosition + new Vector3(0, temp * height, 0);
        overlap.localScale = overlapScale + new Vector2(0, temp);
    }

    //돈나가는 이펙트와 소리 출력해주는 함수
    void MoneyOut()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 position = new Vector3(mousePos.x, mousePos.y, 0);  //마우스위치에 생성(그냥 마우스는 z가 -10이라서 0으로 수정)

        GameObject anim = Instantiate(moneyAni, position, Quaternion.identity) as GameObject;
        anim.transform.SetParent(transform);
        Destroy(anim, anim.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).length);

        //소리 재생
        AudioSource.PlayClipAtPoint(moneySound, Camera.main.transform.position);
    }

    void PlaySound(AudioClip audio)
    {

        //소리 재생
        AudioSource.PlayClipAtPoint(audio, Camera.main.transform.position);
    }*/
    
}