﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestManager : MonoBehaviour {

    public Text moneyText;
    public int money = 10000;
    public int income = 7;

	// Use this for initialization
	void Start () {

        InvokeRepeating("UpdatePerTurn", 0.0f, 0.001f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void UpdatePerTurn()
    {
        UpdateMoney();
    }
    void UpdateMoney()
    {
        money += income;
        moneyText.text = money.ToString();
    }
}