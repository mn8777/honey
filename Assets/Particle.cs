﻿using UnityEngine;
using System.Collections;

public class Particle : MonoBehaviour 
{
	public float Lifespan;

	void Update()
	{
		Lifespan -= Time.deltaTime;
		if (Lifespan < 0) 
		{
			Destroy (gameObject);
		}
	}
}