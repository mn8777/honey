﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slot : MonoBehaviour
{
	public GameManager Manager;
	public Worker MyWorker;
	public List<Slot> AdjoinSlots;

	public int Level;
	public int Wage;
	public bool Available;
	public Worker StaffPrefab;

	void OnMouseDown()
	{
		if (MyWorker == null) 
		{
			MyWorker = Instantiate (StaffPrefab, transform.position + new Vector3 (0, 0, -1), Quaternion.identity) as Worker;
			MyWorker.Manager = Manager;
			MyWorker.MySlot = this;
		}
	}
}